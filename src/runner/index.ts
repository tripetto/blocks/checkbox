/** Dependencies */
import {
    NodeBlock,
    Slots,
    assert,
    castToNumber,
    validator,
} from "@tripetto/runner";
import "./conditions/checkbox";
import "./conditions/score";

export abstract class Checkbox extends NodeBlock<{
    readonly scoreForTrue?: number;
    readonly scoreForFalse?: number;
}> {
    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Contains the checkbox slot. */
    readonly checkboxSlot = assert(
        this.valueOf<boolean, Slots.Boolean>("checked", "static", {
            confirm: true,
            onChange: (checkbox) => {
                if (this.scoreSlot && checkbox.slot instanceof Slots.Boolean) {
                    this.scoreSlot.set(
                        checkbox.hasValue
                            ? castToNumber(
                                  checkbox.value === true
                                      ? this.props.scoreForTrue
                                      : this.props.scoreForFalse
                              )
                            : undefined
                    );
                }
            },
        })
    );

    /** Contains if the checkbox is required. */
    readonly required = this.checkboxSlot.slot.required || false;

    /** Retrieves the current value. */
    get value() {
        return this.checkboxSlot.value;
    }

    /** Sets the current value. */
    set value(value: boolean) {
        this.checkboxSlot.value = value;
    }

    /** Toggles the checkbox. */
    toggle(): void {
        this.checkboxSlot.value = !this.checkboxSlot.value;
    }

    @validator
    validate(): boolean {
        return !this.required || this.checkboxSlot.value === true;
    }
}
