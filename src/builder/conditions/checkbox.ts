/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    Slots,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "@tripetto/builder";

/** Assets */
import ICON from "../../../assets/condition.svg";
import ICON_CHECKED from "../../../assets/checked.svg";
import ICON_UNCHECKED from "../../../assets/unchecked.svg";

@tripetto({
    type: "condition",
    legacyBlock: true,
    context: PACKAGE_NAME,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:checkbox", "Checkbox state");
    },
})
export class CheckboxCondition extends ConditionBlock {
    @definition
    @affects("#name")
    readonly checked: boolean = true;

    get icon() {
        return this.checked ? ICON_CHECKED : ICON_UNCHECKED;
    }

    get name() {
        return this.checked
            ? (this.slot instanceof Slots.Boolean && this.slot.labelForTrue) ||
                  pgettext("block:checkbox", "Checked")
            : (this.slot instanceof Slots.Boolean && this.slot.labelForFalse) ||
                  pgettext("block:checkbox", "Not checked");
    }

    @editor
    defineEditor(): void {
        if (this.node?.label) {
            this.editor.form({
                controls: [
                    new Forms.Checkbox(
                        this.node?.label,
                        Forms.Checkbox.bind(this, "checked", true)
                    ),
                ],
            });
        }
    }
}
